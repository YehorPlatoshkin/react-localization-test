import {render} from 'react-dom';
import './index.css';
import * as React from 'react';
import {ColumnDirective, ColumnsDirective, GridComponent, Inject, Page, Toolbar} from '@syncfusion/ej2-react-grids';
import {categoryData} from './data';
import {SampleBase} from './sample-base';
import {L10n} from '@syncfusion/ej2-base';
import {FormattedMessage, IntlProvider} from 'react-intl';

L10n.load({
    'es': {
        'grid': {
            'EmptyRecord': 'Keine Aufzeichnungen angezeigt',
            'ChooseColumns': 'Elegir columnas',
            'Columnchooser': 'columnas',
            'Search': 'HUIHUIHUI'
        },
        'pager': {
            'currentPageInfo': '{0} de {1} páginas',
            'totalItemsInfo': '({0} elementos)',
            'firstPageTooltip': 'Ir a la primera pagina'
        },

    }

});

const messages = {
    simple: 'Hello world',
    placeholder: 'Hello {name}',
    date: 'Hello {ts, date}',
    time: 'Hello {ts, time}',
    number: 'Hello {num, number}',
    plural: 'I have {num, plural, one {# dog} other {# dogs}}',
    select: 'I am a {gender, select, male {boy} female {girl}}',
    selectordinal: `I am the {order, selectordinal, 
        one {#st person} 
        two {#nd person}
        =3 {#rd person} 
        other {#th person}
    }`,
    richtext: 'I have <bold>{num, plural, one {# dog} other {# dogs}}</bold>',
    richertext:
        'I have & < &nbsp; <bold>{num, plural, one {# & dog} other {# dogs}}</bold>',
    unicode: 'Hello\u0020{placeholder}',
};

export class Searching extends SampleBase {
    grid;

    constructor() {
        super(...arguments);
        this.toolbarOptions = ['Search'];
    }

    render() {
        return (
            <div className='control-pane'>
                <p>
                    <FormattedMessage id="simple"/>
                    <br/>
                    <FormattedMessage id="placeholder" values={{name: 'John'}}/>
                    <br/>
                    <FormattedMessage id="date" values={{ts: Date.now()}}/>
                    <br/>
                </p>
                <div className='control-section row'>
                    <GridComponent ref={g => this.grid = g} load={e => {
                        console.log('grids : ', this.grid);
                        console.log('e : ', e);
                    }}
                                   locale='es' dataSource={categoryData} toolbar={this.toolbarOptions}
                                   allowPaging={true} pageSettings={{pageSize: 10, pageCount: 5}}>
                        <ColumnsDirective>
                            <ColumnDirective field='CategoryName' headerText='Category Name'
                                             width='170'></ColumnDirective>
                            <ColumnDirective field='ProductName' headerText='Product Name'
                                             width='150'></ColumnDirective>
                            <ColumnDirective field='QuantityPerUnit' headerText='Quantity PerUnit' width='180'
                                             textAlign='Right'/>
                            <ColumnDirective field='UnitsInStock' headerText='Units In Stock' width='150'
                                             textAlign='Right'/>
                        </ColumnsDirective>
                        <Inject services={[Toolbar, Page]}/>
                    </GridComponent>
                </div>

            </div>);
    }
}

render(
    <IntlProvider locale="en" messages={messages}>
        <Searching/>
    </IntlProvider>
    , document.getElementById('sample'));